package configuration;


import annotation.Injectable;
import lombok.Data;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.*;

@Data
public class Injector {
    // the injector instance
    private static Injector injector;
    // mapClassBinding : contains each interface and it's class implementation
    private Map<Class<?>, Class<?>> mapClassBinding;
    // contain each interface/class and it's instantiated object
    private Map<Class<?>, Object> mapClassImplementations;

    // private constructor because the injector is singleton
    private Injector() {
        mapClassImplementations = new HashMap< Class<?>, Object >();
        mapClassBinding = new HashMap< Class<?>, Class<?> >();
    }

    public static Injector getInjector() {
        if (injector == null)
            injector = new Injector();
        return injector;
    }

    // adding for each interface it's implementation class
    public void addClassImplementation(Class<?> interface_di, Class<?> class_di) {
        try{
            if(mapClassBinding.get(interface_di) == null) {
              getInjector().mapClassBinding.put(interface_di, class_di);
            }
        }
        catch(Exception e) {
            System.out.println("addClassImplementation exception: " + e.getMessage());
        }
    }

    // get attribute that has injector annotation
    public List<Field> getAnnotatedFields(Class<?> className) {
        List<Field> classFields = new ArrayList<Field>(Arrays.asList(className.getDeclaredFields()));
        List<Field> injectableFields = new ArrayList<Field>();

        for (Field field : classFields) {
            if (field.getDeclaredAnnotations().length > 0) {

                Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
                for (int i = 0; i < fieldAnnotations.length; i++) {
                    if (fieldAnnotations[i].toString().equals("@annotation.Injectable()")) {
                        injectableFields.add(field);
                    }
                }
            }
        }
        return injectableFields;
    }


    // create a new instance of a given class
    public Object newInstance(Class<?> className) throws Exception
    {
        // we verify the type of injection of the class is it attribute or constructor
        AttributeInjectionState attributeInjectionState = getInjectionType(getBindingClass(className));

        Object newObject = null;
        if(attributeInjectionState == AttributeInjectionState.CONSTRUCTOR)
        {

            Constructor injectableConstructor = Arrays.stream(className.getConstructors())
                    .filter(constructor -> constructor.getAnnotation(Injectable.class) != null)
                    .findFirst().get();

            Class<?>[] parameters = injectableConstructor.getParameterTypes();
            Object[] objects = new Object[parameters.length];
            for(int i = 0; i < parameters.length; i++) {
                objects[i] = getObject(parameters[i]);
            }
            newObject = injectableConstructor.newInstance(objects);
        }
        if(attributeInjectionState == AttributeInjectionState.ATTRIBUTE)
        {
            newObject = className.getDeclaredConstructor().newInstance();
            List<Field> injectableFields = getAnnotatedFields(className);


            for(Field injectableField: injectableFields)
            {
                injectableField.setAccessible(true);
                injectableField.set(newObject, getObject(injectableField.getType()));
            }
        }

        return newObject;

    }

    // a recursive function that returns the object of a given class or interface
    // by creating embedded objects
    private Object getObject(Class<?> classOrInterface) throws  Exception
    {
        // the classOrInterface might be a name of a class or interface
        // now I am sure the className variable contain a className
        Class<?> className = getBindingClass(classOrInterface);
        AttributeInjectionState attributeInjectionState = getInjectionType(getBindingClass(className));


        // if it's a constructor injection
        if(attributeInjectionState == AttributeInjectionState.CONSTRUCTOR)
        {
            Constructor injectableConstructor = Arrays.stream(className.getConstructors())
                    .filter(constructor -> constructor.getAnnotation(Injectable.class) != null)
                    .findFirst().get();

            Class<?>[] parameters = injectableConstructor.getParameterTypes();
            Object[] objects = new Object[parameters.length];
            for(int i = 0; i < parameters.length; i++) {
                objects[i] = getObject(parameters[i]);
            }
            mapClassImplementations.put(classOrInterface, injectableConstructor.newInstance(objects));
            return mapClassImplementations.get(classOrInterface);
        }
        // if it's attribute injection
        else if(attributeInjectionState == AttributeInjectionState.ATTRIBUTE)
        {
            if (mapClassImplementations.containsKey(className) == false) {
                if (mapClassBinding.containsKey(className) == true)
                {
                    mapClassImplementations.put(className
                            , mapClassBinding.get(className).getDeclaredConstructor().newInstance());
                }
                else
                {
                    mapClassImplementations.put(className
                            , getBindingClass(className).getDeclaredConstructor().newInstance());
                }
            }

            List<Field> injectableFields = getAnnotatedFields(className);

            for(Field injectableField: injectableFields)
            {
                injectableField.setAccessible(true);
                injectableField.set(mapClassImplementations.get(className), getObject(injectableField.getType()));
            }
            return mapClassImplementations.get(className);
        }
        else
        {
            if(mapClassImplementations.containsKey(classOrInterface) == false){
                if(mapClassBinding.containsKey(classOrInterface) == false)
                    mapClassImplementations.put(classOrInterface, className.getDeclaredConstructor().newInstance());
                else
                    mapClassImplementations.put(classOrInterface, mapClassBinding.get(classOrInterface).getDeclaredConstructor().newInstance());
            }
            return mapClassImplementations.get(classOrInterface);
        }
    }

    // get an implementation class for a given class or interface name
    private Class<?> getBindingClass(Class<?> className)
    {
        Class<?> bindingClass = mapClassBinding.get(className);
        className = (bindingClass != null) ? bindingClass : className;
        return className;
    }

    public AttributeInjectionState getInjectionType(Class<?> className) throws Exception {
        boolean constructorInjection = false;
        boolean attributeInjection = false;

        attributeInjection = (getAnnotatedFields(className).size() > 0);

        for(Constructor constructor : className.getConstructors())
        {
            if(constructor.getAnnotation(Injectable.class) != null)
            {
                if(constructorInjection == true)
                    throw new Exception("Cannot use multiple constructor for injection");
                constructorInjection = true;
            }
        }

        if (constructorInjection && attributeInjection)
            throw new Exception("Cannot use Attribute and Constructor injection at the same time");

        if(constructorInjection)
            return AttributeInjectionState.CONSTRUCTOR;
        if(attributeInjection)
            return AttributeInjectionState.ATTRIBUTE;
        return AttributeInjectionState.NONE;
    }

    // an enumeration for the injection type
    enum AttributeInjectionState {
        ATTRIBUTE,
        CONSTRUCTOR,
        BOTH,
        NONE
    }

}


