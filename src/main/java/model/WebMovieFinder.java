package model;


import annotation.Injectable;
import lombok.Data;

@Data
public class WebMovieFinder implements MovieFinder{
    @Injectable
    AuditService auditService;

//    @Injectable
//    public WebMovieFinder(AuditService auditService)
//    {
//        this.auditService = auditService;
//    }
}
