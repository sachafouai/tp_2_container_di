import configuration.Injector;
import model.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ContainerIDTest {

    Injector injector;

    @BeforeEach
    void createInjector() {
        injector = Injector.getInjector();
    }

    @Test
    @DisplayName("Test simple injection")
    void testSimpleInjection() {
        try {
            injector.addClassImplementation(AuditService.class, SimpleAuditService.class);
            injector.addClassImplementation(MovieFinder.class, WebMovieFinder.class);

            MovieLister movieLister = (MovieLister) injector.newInstance(MovieLister.class);
            assertNotEquals(movieLister.getMovieFinder(), null);
            assertTrue(movieLister.getMovieFinder() instanceof WebMovieFinder);
            assertNotEquals(((WebMovieFinder) movieLister.getMovieFinder()).getAuditService(), null);
            assertTrue(((WebMovieFinder) movieLister.getMovieFinder()).getAuditService() instanceof SimpleAuditService);

            MovieLister movieLister_1 = (MovieLister) injector.newInstance(MovieLister.class);
            assertNotEquals(movieLister_1.getMovieFinder(), null);
            assertTrue(movieLister_1.getMovieFinder() instanceof WebMovieFinder);
            assertNotEquals(((WebMovieFinder) movieLister_1.getMovieFinder()).getAuditService(), null);
            assertTrue(((WebMovieFinder) movieLister_1.getMovieFinder()).getAuditService() instanceof SimpleAuditService);

            assertEquals(injector.getMapClassImplementations().size(), 2);

        } catch (Exception e) {
            fail("Test fail because of binding error. " + e.getMessage());
        }
    }
}
